import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    users: [
      {
        id: 1,
        name: 'Pasha',
        phone: '0986756789',
        surname: 'Banker',
        email: 'pranker@gmail.com',
        birthday: '10.08.1980'
      },
      {
        id: 2,
        name: 'Vlad',
        phone: '0986756789',
        surname: 'Banker',
        email: 'prankerRRA54@gmail.com',
        birthday: '10.08.1999'
      },
      {
        id: 3,
        name: 'Ken',
        phone: '0986756789',
        surname: 'Ivanov',
        email: 'kenJG43@gmail.com',
        birthday: '10.08.1977'
      },
      {
        id: 4,
        name: 'Oleg',
        phone: '0986756789',
        surname: 'Ivanov',
        email: 'oleg34535@gmail.com',
        birthday: '10.08.1989'
      },
      {
        id: 5,
        name: 'Homelander',
        phone: '0986756789',
        surname: 'Petrov',
        email: 'homelander007@gmail.com',
        birthday: '10.08.1977'
      },
      {
        id: 6,
        name: 'Deep',
        phone: '0986756789',
        surname: 'Petrov',
        email: 'deep007@gmail.com',
        birthday: '10.08.1989'
      },
      {
        id: 7,
        name: 'Butcher',
        phone: '0986756789',
        surname: 'Tarasov',
        email: 'butcher007@gmail.com',
        birthday: '10.08.1977'
      },
      {
        id: 8,
        name: 'Evgeniy',
        phone: '0986756789',
        surname: 'Tarasov',
        email: 'evgeniy007@gmail.com',
        birthday: '10.08.1989'
      },
      {
        id: 10,
        name: 'Alex',
        phone: '0986756789',
        surname: 'Gerasimov',
        email: 'alex007@gmail.com',
        birthday: '10.08.1977'
      },
      {
        id: 11,
        name: 'Gena',
        phone: '0986756789',
        surname: 'Gerasimov',
        email: 'gena007@gmail.com',
        birthday: '10.08.1989'
      }
    ],
  },
  getters: {
  },
  mutations: {
    ADD_USER(state, data) {
      // state.users.push(data)
      state.users.splice(0, 0, data)
    },
    DELETE_USER(state,index) {
      console.info('Delete user', state.users[index])
      state.users.splice(index, 1)
    },
    UPDATE_USER(state, {user, index}) {
      console.info('Update user', state.users[index])
      Vue.set(state.users, index, user)
      // state.users[index] = user
    },
  },
  actions: {
    addUser({commit}, data){
      commit('ADD_USER', {...data, id: Date.now()})
    },
    deleteById({commit, state}, id) {
      const index = state.users.findIndex(user => user.id == id)
      if(index > -1) {
        commit('DELETE_USER', index)
      }
    },
    updateUser({commit, state}, user) {
      const index = state.users.findIndex(u => u.id == user.id)
      if(index > -1) {
        commit('UPDATE_USER', {user, index})
      }
    }
  },
  plugins: [createPersistedState()]
})
